import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import routes from "../constants/routes.json";
import styles from "./Home.css";
import aliasImage from "../images/aliasman.png";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import MaterialTable from "material-table";
import PhoneIcon from "@material-ui/icons/Phone";
import OpenInBrowserIcon from "@material-ui/icons/OpenInBrowser";
import LocalShippingIcon from "@material-ui/icons/LocalShipping";
import InfoIcon from "@material-ui/icons/Info";
import ImportExportIcon from "@material-ui/icons/LocalShipping";
import os from "os";
import ControlledExpansionPanels from "./About";
import { APP_VERSION } from "../constants/version";
import Import from "./Import";
import shellJs from "shelljs";
import AlTable from "./Table";
const execSync = require("child_process").execSync;

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}
export default function Home() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };
  return (
    <div className={styles.container} data-tid="container">
      <Grid container component={"div"} spacing={3}>
        <Grid component={"div"} item>
          <img src={aliasImage} height="50px" width="50px"></img>
        </Grid>
        <Grid component={"div"} item>
          <Typography variant="h5">{` AliasMan ${APP_VERSION}`}</Typography>
          <Typography variant="subtitle1">{`os:${os.platform()}:${os.release()}`}</Typography>
        </Grid>
      </Grid>
      <br />
      <Paper square>
        <Tabs
          value={value}
          indicatorColor="primary"
          textColor="primary"
          onChange={handleChange}
          aria-label="disabled tabs example"
        >
          <Tab label="Active" icon={<OpenInBrowserIcon />} />
          <Tab label="Export/Import" icon={<ImportExportIcon />} />
          <Tab label="About" icon={<InfoIcon />} />
        </Tabs>

        <TabPanel value={value} index={0}>
          <AlTable />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Import />
        </TabPanel>
        <TabPanel value={value} index={2}>
          <ControlledExpansionPanels />
        </TabPanel>
      </Paper>
    </div>
  );
}
