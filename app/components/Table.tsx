import React, { useEffect } from "react";
import fs from "fs";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MaterialTable from "material-table";
import { IAliasData } from "./interface";
import Grid from "@material-ui/core/Grid";
import AddIcon from "@material-ui/icons/Add";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { addAlias } from "../utils/alias";
const lineByLine = require("n-readlines");
const { app } = require("electron").remote;
const execSync = require("child_process").execSync;

export default function AlTable() {
  const [expanded, setExpanded] = React.useState<string | false>(false);
  const [aliState, setAliases] = React.useState([]);
  const [open, setOpen] = React.useState(false);
  const [aname, setAname] = React.useState("");
  const [acmd, setAcmd] = React.useState("");
  const [fire, setFire] = React.useState("");

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleDelete = async (name: string, command: string) => {
    try {
      await deleteAlias(name, command);
      setFire(name);
    } catch (e) {
      console.log(e);
    } finally {
      console.log("here");
      readAndSetAliases();
    }
  };

  const deleteAlias = async (name: string, command: string) => {
    let cmd = `alias ${name}="${command}"`;
    deleteAliasFromFile(`${app.getPath("home")}/.aliasman.aliases`, cmd);
    let refreshCmd = `source ~/.bash_profile`;
    var child = execSync(refreshCmd);
  };

  const deleteAliasFromFile = async (fileName: string, alias: string) => {
    fs.readFile(fileName, { encoding: "utf-8" }, function(err, data) {
      if (err) throw error;

      let dataArray = data.split("\n"); // convert file data in an array
      const searchKeyword = alias; // we are looking for a line, contains, key word 'user1' in the file
      let lastIndex = -1; // let say, we have not found the keyword

      for (let index = 0; index < dataArray.length; index++) {
        if (dataArray[index].includes(searchKeyword)) {
          // check if a line contains the 'user1' keyword
          lastIndex = index; // found a line includes a 'user1' keyword
          break;
        }
      }

      dataArray.splice(lastIndex, 1); // remove the keyword 'user1' from the data Array

      // UPDATE FILE WITH NEW DATA
      // IN CASE YOU WANT TO UPDATE THE CONTENT IN YOUR FILE
      // THIS WILL REMOVE THE LINE CONTAINS 'user1' IN YOUR shuffle.txt FILE
      const updatedData = dataArray.join("\n");
      fs.writeFile(fileName, updatedData, err => {
        if (err) throw err;
        readAndSetAliases();
      });
    });
  };

  const submitClose = async () => {
    await addAlias(aname, acmd);
    readAndSetAliases();
    console.log(">>>>>asd", aname, acmd);
    setOpen(false);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const checkAndAddLink = () => {
    let filePath = `${app.getPath("home")}/.aliasman.aliases`;
    if (fs.existsSync(filePath)) {
      console.log("leaving");
    } else {
      console.log("add alias");
      let cmd = `source ~/.aliasman.aliases\n`;
      fs.appendFileSync(`${app.getPath("home")}/.bash_profile`, cmd);
      fs.closeSync(fs.openSync(filePath, "w"));
    }
  };

  const readAndSetAliases = () => {
    const totalAlias: IAliasData[] = [];
    let filePath = `${app.getPath("home")}/.aliasman.aliases`;
    if (fs.existsSync(filePath)) {
      const liner = new lineByLine(`${app.getPath("home")}/.aliasman.aliases`);
      let line;
      while ((line = liner.next())) {
        let convertedline = line.toString();
        let aliasNameFull = `${convertedline}`.substr(
          6,
          convertedline.indexOf("=")
        );
        let commandCutLength = aliasNameFull.length - 1;
        let commandFake = convertedline.substr(
          convertedline.indexOf("=") + 2,
          convertedline.length
        );

        console.log("len", commandFake);
        let aliasObject = {
          command: commandFake.slice(0, -1),
          name: aliasNameFull.substr(0, aliasNameFull.indexOf("="))
        };
        totalAlias.push(aliasObject);
      }

      console.log("tlais", app.getPath("home"));
      setAliases(totalAlias);
    }
  };

  useEffect(() => {
    checkAndAddLink();
    readAndSetAliases();
  }, []);

  return (
    <div>
      <Grid constainer style={{ padding: "10px" }}>
        <Grid item>
          <Button
            onClick={() => handleClickOpen()}
            variant="outlined"
            size="small"
            color="primary"
            startIcon={<AddIcon />}
          >
            Add Alias
          </Button>
        </Grid>
      </Grid>

      <MaterialTable
        title="Current Aliases"
        options={{ pageSizeOptions: [5] }}
        columns={[
          { title: "Alias", field: "name" },
          { title: "Command", field: "command" }
        ]}
        data={aliState}
        actions={[
          {
            icon: "delete",
            tooltip: "Delete User",
            onClick: async (event, rowData) => {
              let result = confirm(
                "Are You Sure to Delete Alias " + rowData.name
              );
              result && (await handleDelete(rowData.name, rowData.command));
              readAndSetAliases();
            }
          }
        ]}
      />

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Add Alias</DialogTitle>
        <DialogContent>
          <TextField
            onChange={event => setAname(event.target.value)}
            autoFocus
            margin="dense"
            id="name"
            label="Alias Name"
            type="text"
            fullWidth
          />
          <TextField
            onChange={event => setAcmd(event.target.value)}
            autoFocus
            margin="dense"
            id="name"
            label="Command"
            type="text"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={submitClose} color="primary" variant="contained">
            Add Alias
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
