import React, { useEffect } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import MenuOpenIcon from "@material-ui/icons/MenuOpen";
import BackupIcon from "@material-ui/icons/Backup";
const lineByLine = require("n-readlines");
const { app, dialog } = require("electron").remote;
import fs from "fs";
import { addAlias } from "../utils/alias";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      paddingTop: "20px"
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: "33.33%",
      flexShrink: 0
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary
    }
  })
);

export default function Import() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState<string | false>(false);
  const [aliState, setAliases] = React.useState([]);

  const handleClickOpen = () => {
    try {
      console.log("Export Clicked");
      const dg = dialog.showSaveDialogSync({
        title: "Export Alias",
        filters: [{ name: `export-aliasman`, ext: ["export"] }] // what kind of files do you want to see when this box is opend
      });
      fs.writeFileSync(dg, JSON.stringify(aliState));
      let myNotification = new Notification("Export Complete ", {
        body: "Your File has been Exported"
      });
    } catch (e) {}
  };

  const handleImport = () => {
    console.log("Export Clicked");
    try {
      const dg = dialog.showOpenDialogSync();
      const imFile = fs.readFileSync(dg[0], { encoding: "utf8" });
      const parsedFile = JSON.parse(imFile);
      parsedFile.forEach(element => {
        addAlias(element.name, element.command);
      });
      let myNotification = new Notification("Import Complete ", {
        body: "Your File has been Imported"
      });
    } catch (e) {
      let myNotification = new Notification("Import Failed ", {
        body: "Your File doesn't supports aliasman format"
      });
    }
  };

  const readAndSetAliases = () => {
    const totalAlias: IAliasData[] = [];
    const liner = new lineByLine(`${app.getPath("home")}/.aliasman.aliases`);
    let line;
    while ((line = liner.next())) {
      let convertedline = line.toString();
      let aliasNameFull = `${convertedline}`.substr(
        6,
        convertedline.indexOf("=")
      );
      let commandCutLength = aliasNameFull.length - 1;
      let commandFake = convertedline.substr(
        convertedline.indexOf("=") + 2,
        convertedline.length
      );

      console.log("len", commandFake);
      let aliasObject = {
        command: commandFake.slice(0, -1),
        name: aliasNameFull.substr(0, aliasNameFull.indexOf("="))
      };
      totalAlias.push(aliasObject);
    }

    console.log("tlais", totalAlias);
    setAliases(totalAlias);
  };

  useEffect(() => {
    readAndSetAliases();
  }, []);

  const handleChange = (panel: string) => (
    event: React.ChangeEvent<{}>,
    isExpanded: boolean
  ) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <div className={classes.root}>
      <ExpansionPanel
        expanded={expanded === "panel1"}
        onChange={handleChange("panel1")}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography component={"div"} className={classes.heading}>
            Import
          </Typography>
          <Typography component={"div"} className={classes.secondaryHeading}>
            Import Aliases From the Alias file
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Grid container>
            <Grid item xs={4}>
              <Button
                color="primary"
                variant="contained"
                startIcon={<BackupIcon />}
                onClick={handleImport}
              >
                {`Select And Import`}
              </Button>
            </Grid>
            <Grid item xs={2}></Grid>
          </Grid>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel
        expanded={expanded === "panel2"}
        onChange={handleChange("panel2")}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2bh-content"
          id="panel2bh-header"
        >
          <Typography component={"div"} className={classes.heading}>
            Export
          </Typography>
          <Typography component={"div"} className={classes.secondaryHeading}>
            Export Aliases From the Alias file
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Button
            color="primary"
            variant="contained"
            startIcon={<MenuOpenIcon />}
            onClick={handleClickOpen}
          >
            Export Alias
          </Button>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  );
}
